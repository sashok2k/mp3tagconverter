﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TagConvert
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputFolder = String.Empty;

            if (args.Length > 0)
            {
                inputFolder = args[0];
            }

            if (inputFolder.Equals(String.Empty))
            {
                PrintUsage();
                return;
            }

            if (!Directory.Exists(inputFolder))
            {
                Console.WriteLine("Wrong folder name");

                PrintUsage();
                return;
            }

            EnumFolders(inputFolder);
        }

        private static void PrintUsage()
        {
            Console.WriteLine("");
            Console.WriteLine("Usage: TagConvert.exe {Folder Name}");
            
        }

        private static void EnumFolders(string folder)
        {
            Directory.GetFiles(folder, "*.mp3").ToList().ForEach(file => { 
                ConvertTags(file); 
            });
            Directory.GetDirectories(folder).ToList().ForEach(f =>
            {
                EnumFolders(f);
            });
        }

        private static void ConvertTags(string fileName)
        {
            Console.WriteLine(fileName);

            var attr=File.GetAttributes(fileName);
            // Remove Read Only Attribute
            if ((attr & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                File.SetAttributes(fileName, attr & ~FileAttributes.ReadOnly);
            
            TagLib.File file = TagLib.File.Create(fileName);
            
            file.Tag.Artists = Convert(file.Tag.Artists);
            file.Tag.Title = Convert(file.Tag.Title);
            file.Tag.Album = Convert(file.Tag.Album);
            file.Tag.Comment = Convert(file.Tag.Comment);
            file.Tag.AlbumArtists = Convert(file.Tag.AlbumArtists);
            file.Tag.Composers = Convert(file.Tag.Composers);
            file.Tag.Performers = Convert(file.Tag.Performers);
            file.Tag.Genres = Convert(file.Tag.Genres);

            file.Save();
        }

        static string[] Convert(string[] inpit)
        {
            if (inpit == null)
                return inpit;
            if (inpit.Length == 0)
                return inpit;

            List<string> lInput = inpit.ToList();

            return lInput.ConvertAll(x => Convert(x)).ToArray();
        }

        static string Convert(string input)
        {
            if (input == null)
                return input;

            if (!NeedConversion(input))
                return input;

            var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(input);
            return Encoding.GetEncoding(1251).GetString(bytes);
        }

        /// <summary>
        /// Checks is any Russian character
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        static bool NeedConversion(string input)
        {
            return Encoding.GetEncoding("iso-8859-1").GetBytes(input).Any(x => x > 192);
        }
    }
}
